using _04_Static;

var carRepair = new Invoice(345.50m, DateTime.Now.AddDays(5));
var electricity = new Invoice(26.64m, DateTime.Now.AddDays(-2));

AccountingHelpers.DisplayLocalizedInvoice(carRepair, CountrySettings.CurrencySuffix, CountrySettings.DecimalPlaces);
AccountingHelpers.DisplayLocalizedInvoice(electricity, CountrySettings.CurrencySuffix, CountrySettings.DecimalPlaces);

/*
 * 01 - Seznamte se s programem. Spusťte.
 * Rozšiřte program tak, aby používal i aktuální (dnešní) kurz. Aby při výpisu faktury převáděl EUR na Kč.
 */


/*
 * 02 - Přečtěte si https://gitlab.com/school9961818/objects/-/blob/master/Static.md
 */

/*
 * 03 - Vytvořte novou třídu (v novém souboru), která bude reprezentovat Vaše náklady.
 * Bude možné v ní uchovávat libovolné množství faktur (Invoice[]).
 * Vytvořte objekt pro Vaše náklady a uložte do něj alespoň 5 různých faktur v rozmezí alespoň 60 dní.
 */

/*
 * 04 - Rozšiřte třídu s náklady o metodu, která vrátí všechny faktury (Invoice[]) s prošlým datem zaplacení.
 * Vypište je na obrazovku (foreach cyklus).
 */

/*
 * 05 - Rozšiřte třídu s náklady o metodu, která vrátí všechny faktury s budoucím datem zaplacení.
 * Vypište je na obrazovku.
 */

/*
 * 06 - Rozšiřte třídu s náklady o metodu, která vrátí celkový obnos peněz (v Kč), které ještě čekají na zaplacení.
 * Vypište na obrazovku.
 */
