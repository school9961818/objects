﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Static
{
    public static class CountrySettings
    {
        public static string CurrencySuffix { get; } = "Kč";

        public static int DecimalPlaces { get; } = 0;
    }
}
