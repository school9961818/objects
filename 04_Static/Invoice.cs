﻿namespace _04_Static
{
    public class Invoice
    {
        public Invoice(decimal priceInEuro, DateTime dueDate)
        {
            PriceInEuro = priceInEuro;
            DueDate = dueDate;         
        }

        public decimal PriceInEuro { get; init; }

        public DateTime DueDate { get; init; }
    }
}
