﻿using System;

namespace _04_Static
{
    public static class AccountingHelpers
    {
        public static void DisplayLocalizedInvoice(Invoice invoice, string currencySuffix, int decimalPlaces)
        {
            Console.WriteLine($"This invoice is for {Math.Round(invoice.PriceInEuro, decimalPlaces)} {currencySuffix}.");
            Console.WriteLine($"Due date is {invoice.DueDate.ToShortDateString()}.");
            if (DateTime.Now > invoice.DueDate)
            {
                Console.WriteLine("Too late to pay now.");
            }
            else
            {
                Console.WriteLine($"There are still {(invoice.DueDate - DateTime.Now).Days} days to pay.");
            }
            Console.WriteLine();
        }
    }
}
