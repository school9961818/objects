﻿## Constructor

Je speciální metoda, která se volá při vytváření nového objektu.

``` csharp
public class Person
{
    private DateTime dateOfBirth; // toto je field


    public Person(DateTime dateOfBirth) // toto je constructor
    {
        this.dateOfBirth = dateOfBirth;
    }
}
```

> Constructor se musí jmenovat stejně jako třída

Construktor se pak zavolá při každém vytváření nového objektu.

```csharp
var newborn = new Person(DateTime.Now);
```

Tímto přístupem zařídíme:

- [x] objekt nelze vytvořit bez klíčových dat (parametry konstruktoru)
- [x] klíčová data po vytvoření nelze měnit (po nastavení v konstruktoru už jsou `private`)

***

Všimněte si také slova `this` uvnitř constructoru. Ten rozlišuje, jestli pracujeme s fieldem dané třídy (pak tam je `this`) a nebo jestli pracujeme se vstupním parametrem.

Alternativou je použít různé názvy:

``` csharp
public class Person
{
    private DateTime dateOfBirth; // toto je field


    public Person(DateTime bornAt) // toto je constructor
    {
        dateOfBirth = bornAt;
    }
}
```

Obecně ale platí, že je lepší použít `this`, než komolit a vymýšlet extra názvy pro stejnou věc.
