﻿using Objects;

var volkswagen = new Car()
{
    TankCapacity = 45,
    Consumption = 6.3m
};

Console.WriteLine($"The maximum range of my VW is: {volkswagen.GetMaxRange()} km");

/*
 * 01 - Prostudujte si a spusťte program. 
 * Vytvořte nový objekt (proměnnou) třídy (typu) "Car" a vypište na obrazovku maximální dojezd. 
 * Typ auta zvolte dle vlastního výběru.
 */

/*
 * 02 - Přidejte do třídy "Car" novou vlastnost, která bude reprezentovat rezervu v nádrži (v litrech).
 * Rezerva bude navíc k již existující nádrži.
 * Upravte metodu pro výpočet dojezdu tak, aby počítala jak s nádrží tak s rezervou.
 * Rozšiřte vytváření předchozích dvou aut o rezervu (větší než 0). 
 */

/*
 * 03 - Přidejte do třídy "Car" novou metodu
 *   vstupní parametry 
 *     cena za litr
 *   výsupní parametry
 *     cena natankování prázdného auta (nádrž i rezerva)
 *   
 *   Vypište na obrazovku, kolik stojí natankovat obě auta.
 */

/*
 * 04 - Přidejte do třídy "Car" novou metodu
 *   vstupní parametry 
 *     požadovaná vzdálenost v km
 *   výsupní parametry
 *     chybějící litry pro danou vzdálenost (0 pokud na plnou nádrž s rezervou vzdálenost dojedeme)
 *   
 *   Vypište na obrazovku, kolik litrů chybí na cestu z Prahy do Londýna pro obě auta.
 */

/*
 * 05 - Porovnejte a vypište na obrazovku, které auto má větší nádrž.
 */

/*
 * 06 - Porovnejte a vypište na obrazovku, které auto má větší maximální dojezd.
 */

/*
 * 07 - Vytvořte novou třídu pro reprezentaci člověka. Člověk bude mít věk a jméno. 
 * Třídy také vytváříme ve vlastních souborech (inspirujte se třídou "Car").
 * Vytvořte objekt nové třídy, který bude reprezentovat Vás.
 * Na obrazovku vypište jméno a věk Vaší osoby.
 */
