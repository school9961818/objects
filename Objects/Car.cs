﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public class Car
    {
        public int TankCapacity;
        public decimal Consumption;

        public decimal GetMaxRange()
        { 
            return Math.Round(TankCapacity * Consumption, 2); 
        }
    }
}
