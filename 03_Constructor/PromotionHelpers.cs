﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Constructor
{
    public static class PromotionHelpers
    {
        public static void ApplyTwoYearLoyaltyBonus(SteamAccount account)
        {
            if (account.GetYearsActive() > 1)
            {
                Console.WriteLine($"Congratulations! Your account registered at {account.CreatedAt.ToShortDateString()} is eligible for two year loyalty bonus.");
            }
            else
            {
                Console.WriteLine($"Sorry. This account registered at {account.CreatedAt.ToShortDateString()} is not yet eligible for a two year loyalty bonus.");
            }
        }
    }
}


