﻿namespace _03_Constructor
{
    public class SteamAccount
    {
        public DateTime CreatedAt { get; set; }

        public int GetYearsActive()
        {
            return DateTime.UtcNow.Year - CreatedAt.Year;
        }
    }
}
