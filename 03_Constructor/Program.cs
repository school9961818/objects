using _03_Constructor;

var oldAccount = new SteamAccount()
{
    CreatedAt = new DateTime(2003, 01, 01)
};

var newAccount = new SteamAccount()
{ 
    CreatedAt = DateTime.Now.AddDays(-1) 
};

PromotionHelpers.ApplyTwoYearLoyaltyBonus(oldAccount);
PromotionHelpers.ApplyTwoYearLoyaltyBonus(newAccount);

/*
 * 01 - Seznamte se s kódem. Spusťte program. 
 * Upravte nový účet tak, aby také dosáhl na odměnu za dva roky.
 */

/*
 * Některé hodnoty (např. datum vytvoření nebo datum narození) by se neměly vůbec editovat.
 * Není pro to jediný důvod; naopak jejich změna může být nebezpečná (podvody a jiné).
 * Jak tedy docílit, aby se daná hodnota nastavila PRÁVĚ JEDNOU?
 * 
 * Přečtěte si https://gitlab.com/school9961818/objects/-/blob/master/Constructor.md
 */

/*
 * 02 - Upravte třídu "SteamAccount"
 * Nahraďte public property za private field (nezapomeňte na malé písmeno).
 * Přidejte konstruktor se vstupním parametrem pro datum vytvoření.
 * Upravte vytváření pro "oldAccount" a "newAccount"
 */

/*
 * 03 - Vytvořte novou třídu "SteamGame" s následujícími vlastnostmi. Vše budou property.
 *  - Jméno
 *  - Cena
 *  - Počet nahraných hodin
 *  
 *  Přidejte constructor, který bude vyžadovat v parametrech jméno a cenu.
 *  Uvnitř konstruktoru nastavte všechny property (počet nahraných hodin začne na 0).
 */

/*
 * 04 - Vytvořte tři nové objekty pro tři různé hry.
 */

/*
 * 05 - Upravte třídu "SteamAccount"
 * Přidejte property pro seznam zakoupených her ("public SteamGame[]")
 * Přidejte starému účtu do zakoupených her všechny tři vytvořené objekty třídy "SteamGame" 
 */

/*
 * 06 - Upravte třídu "SteamAccount"
 * Přidejte metodu, která vrátí hodnotu daného ůčtu (cenu všech zakoupených her)
 * Vypište na obrazovku, jakou cenu má starý účet.
 */

/*
 * 07 - Upravte třídu "SteamAccount"
 * Přidejte metodu, která vrátí celkový počet nahraných hodin (na všech hrách)
 * Vypište na obrazovku, kolik nahrál starý účet hodin.
 */

/*
 * 08 - Upravte třídu "SteamGame"
 * Přidejte metodu, která zvýší počet nahraných hodin
 * Vstupní parametr
 *   počet nahraných hodin
 * 
 * Zavolejte metodu s různými parametry na každé ze tří her.
 * Pro jednu z her metodu zavolejte ještě jednou.
 * 
 * Vypište na obrazovku, kolik nahrál starý účet hodin.
 * Sedí to? Pokud ne, ptejte se.
 */

/*
 * 07 - Přidejte jednu hru do nového účtu.
 * Vypište na obrazovku, kolik nahrál nový účet hodin.
 * Mělo by to být 0. 
 * 
 * (Nezapomeňte, že objekty jsou předávány odkazem. Pokud jste použili jednu z existujících her, pak oba účty se odkazují na stejnou hru. Bacha na to.)
 */

/*
 * 08 - Přidejte pár hodin hry na nový účet.
 * Udělejte to skrz objekt účtu.
 * Mělo by to vypadat nějak takto (názvy jak bych dal já):
 * 
 *   newAccount.PurchasedGames.First().PlayFor(3);
 *   
 *     případně
 *     
 *   newAccount.PurchasedGames[0].PlayFor(3);
 */

