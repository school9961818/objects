﻿

## Prerequisites

[Arrays](https://gitlab.com/school9961818/arrays)

## Goal

* Code
  * Class
  * Property
  * Field
  * Constructor
  * Static
  * Inheritance
  * Interface

## Diffuculty

* Intermediate
* 6-8x45 min

## Evolution of programming

S pomocí struktur a polí už bylo možné psát opravdu velké programy. Operační systémy, první větší hry a jiné. Čím větší a delší ale programy byly, tím větší v nich byl zmatek. Hledala se cesta ven.

### Example

S pomocí vlastních datových typů můžeme snadno nadefinovat třeba obdélník:

``` csharp
public record Rectangle
{
    public int A; 
    public int B;
}   
```

A poté napsat metody pro výpočet obvodu a obsahu

``` csharp

public int GetPerimeter(Rectangle rectangle)
{ 
    return 2 * rectangle.A + 2 * rectangle.B; 
}   

public int GetSurfaceArea(Rectangle rectangle)
{ 
    return rectangle.A * rectangle.B; 
}
```

A teď si představte libovolný modelující software (AutoCad, Blender, SketchUp...). Každý z nich pracuje s mnoha geometrickými tvary. Jasně, definice samotných tvarů není problém, ale co metody pro výpočet obsahu a obvodu? Jak je rozlišit? 

Nabízí se `GetPerimeterOfTriangle`, `GetPerimeterOfCircle`, a tak dále...

Ačkoliv funkční, tenhle přístup brzy kolidoval se základním principem:

> Kód by měl být stručný, přehledný a čitelný.

Na scénu přišly objekty.

## Object Oriented Programming

Vychází, jak název napovídá, z reálného světa. Program by měl mít možnost co nejpřesněji reprezentovat vše (objekty) v reálném světě. A to nejen jak _vypadají_, ale i jak se _chovají_.

V objektovém programování lze logicky uspořádat dohromady vlastnosti (properties) a chování (methods). Vypadá to následovně.

```csharp
public class Rectangle
{
    public int A; 
    public int B;

    public int GetPerimeter()
    {
        return 2 * A + 2 * B;
    }

    public int GetSurfaceArea()
    {
        return A * B;
    }
}
```

Místo `record` použijeme `class` (třída). V třídách můžeme definovat jak vlastnosti (strany `A` a `B`), tak metody (`GetPerimeter` a `GetSurfaceArea`).

Jak to potom použijeme v kódu? Např. takto:

```csharp

var rectangle = new Rectangle() 
{ 
    A = 3, 
    B = 3 
};

int perimeter = rectangle.GetPerimeter();

int surfaceArea = rectangle.GetSurfaceArea();

```

***

Objekty jsou předávány odkazem, takže při vytváření nových proměnných je vždy potřeba použít `new`.

Přímo na proměnné pak můžeme (s pomocí `.`) volat metody: `rectangle.GetPerimeter()`. 

Objektové programování umožňuje logicky uspořádat hodnoty s metodami a tím udržet kód přehlednější a čitelnější. Objektové programování zatím není přežité (používá se dodnes). Ve všech majoritních programovacích jazycích (Java, C#, python,...)

### Terminology

* _Datový typ_ určuje množinu hodnot, kterých může proměnná nabývat (`int` pro celá čísla, `string` pro text,...). 
* _Proměnná_ určuje konkrétní hodnotu v rozmezí datového typu


S příchodem OOP (Object Oriented Programming) tyto definice přestaly stačit. Protože  `class` definuje nejen množinu hodnot, ale i metody. Nejsou to tedy už _jen_ datový typ a _jen_ proměnné.

Začalo se jim říkat 
* _třídy_ určují hodnoty a metody
* _objekty_ jsou instancí nějaké třídy (nositelé hodnot a vykonavatelé metod). 

> Ale je to v podstatě pořád to samé. 

[Opakování z jiného zdroje](https://www.itnetwork.cz/csharp/oop/c-sharp-tutorial-uvod-do-objektove-orientovaneho-programovani)
 
## Instructions

Naklonujte si projekt a postupně vyřešte všechna zadání.
