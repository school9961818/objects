## Inheritance limit

Dědičnost se brzy ukázala jako nedostatečná. Nejvíc limitující byla možnost __právě jednoho__ předka. A v některých případech bylo náročné vymyslet rodokmen (v AutoCadu budou dědit šrouby od hřídelí? Naopak? Každá součástka zvlášť?)

Jako odpověď vznikl:

## Interface

``` csharp
public interface IAnimal
{
    void MakeSound();
}
```

Interface si můžete představit jako šablonu nebo USBčko. Definuje rozhraní. Jak vypadá samotná implemenatace (co do USB strkáte) ho nezajímá. To řeší až třída.

``` csharp
public class Horse : IAnimal
{
    public void MakeSound()
    {
        Console.Writeline("Neigh!");
    }
}
```

Zároveň `interface` umožňuje dědičnost. Funguje to velmi podobně jako u tříd.

``` csharp
public interface IHunter : IAnimal
{
    string GetMethodOfHunting();
}

public class Eagle : IHunter
{
    public void MakeSound()
    {
        Console.Writeline("Scree!");
    }

    public string GetMethodOfHunting()
    {
        return "Claws";
    }
}
```

A vůbec největší bonus je, že třída nemá limit na `interface` předky.

``` csharp
public class SwissArmyKnife : ISaw, IScrewDriver, ICanOpener, ITweezers,...
{
    // Lots of Properties and Methods...
}
```

### Advantages

Interface vyřešil veškeré bolístky dědičnosti. Návrh kódu najednou nebyl o vytváření složitých rodokmenů, ale o hledání společných vlastností. To je mnohem jednodušší.

Zároveň je v drtivé většině případů výhodnější použít `interface` než dědičnost, takže má rada začínajícím programátorům by byla:

> Používejte interface vždy (a vyhněte se dědičnosti mezi třídami).

Více k tématu [zde](https://www.reddit.com/r/csharp/comments/qrff37/when_would_you_use_an_interface_instead_of/)

### Interface rules
> Vždy začíná velkým "I"

A taky ho vždy dáváme do vlastního souboru (viz cvičení)

***

> Abstraktní šablona

Z `interface` nelze vytvářet proměnné

