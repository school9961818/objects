﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Encapsulation
{
    public class BankAccount
    {
        private decimal balance = 0; // tímto říkám, že každý nový objekt této třídy začne s hodnotou 0

        public decimal GetBalance() 
        { 
            return balance; 
        }
    }
}
