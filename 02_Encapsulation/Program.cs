﻿/*
 * Přečtěte si: https://gitlab.com/school9961818/objects/-/blob/master/OOPPrinciples.md
 */

using _02_Encapsulation;

var myAccount = new BankAccount();

Console.WriteLine($"My account balance is: {myAccount.GetBalance()}");

/*
 * 01 - Prostudujte si kód. Nyní do třídy "BankAccount" přidejte metodu pro vložení peněz na účet.
 *   Vstupní parametry
 *     vkládaný obnos
 *   Výstupní parametry
 *     žádné
 * 
 * V rámci metody kontrolujte, že obnos je větší jak 0. 
 * Pokud se někdo pokusí vložit zápornou nebo nulovou hodnotu, vypište na obrazovku, že to nejde a nic s "balance" nedělejte
 * 
 * Zkuste vložit 200.
 * Zkuste vložit -100.
 * Vypište na obrazovku hodnotu Vašeho účtu.
 */

/*
 * 02 - Do třídy "BankAccount" Přidejte metodu pro výběr peněz z účtu.
 *   Vstupní parametry
 *     kolik chci vybrat
 *   Výstupní parametry
 *     kolik bylo vybráno
 * 
 * V rámci metody kontrolujte, že máme dost peněz. 
 * Pokud se někdo pokusí vybrat víc, než je na účtu, vypište na obrazovku, že to nejde, a vraťte 0.
 * 
 * Zkuste vybrat 150
 * Zkuste vybrat 150
 * Vypište na obrazovku hodnotu Vašeho účtu.
 */

/*
 * 03 - Lepší než konkurence.
 * Banka chce nabídnout zákazníkům, aby mohli jít do mínusu, ale jen jednou!
 * Pokud jsou v mínusu, nemůžou vybírat, dokud se nedostanou zase do plusu.
 * Příklad:
 *   Stav účtu: 50 -> výběr 100 -> OK
 *   Stav účtu: -20 -> výběr 100 -> FAIL
 * 
 * Upravte metodu pro výběr peněz. 
 * Otestujte Vaše řešení.
 */

/*
 * 04 - Hupsík dupsík, lidi zneužívají naši novinku.
 * Přidejte do metody pro výběr peněz limit pro výběr.
 * Maximální obnos k výběru bude 10 000.
 * Otestujte Vaše řešení (nejde se dostat níž, než -10 000).
 */

/*
 * Take away:
 *   "private" používáme ve chvíli, kdy k nějaké hodnotě nechceme dát neomezený přístup. 
 *   Kdy třeba chceme, aby byla jen pro čtení (GetBalance)
 *   Nebo když chceme validovat, že se s hodnotou zachází správně (limit pro výběr, záporný zústatek, datum narození v budoucnu, atd atd...)
 *   Omezováním a kontrolováním přístupu (zapouzdřením) mimo jiné předcházíme chybám a nevalidním stavům.
 *   
 *   
 * 05 - Přečtěte si: https://gitlab.com/school9961818/objects/-/blob/master/Properties.md
 */

/*
 * TODO 10.11.2023 
 *   - Prepare UnitTest project & infrastructure
 *   - Do it right or divide it by projects?
 *   - AB test with students? How?
 */
