﻿## OOP Principles

Objektové programování se rychle stalo standardem v programování. A protože ho používali všichni a všude, bylo potřeba zavést alespoň základní principy. Vysvětlíme si tři:

* Zapouzdření (Encapsulation)
* Dědičnost (Inheritance)
* Polymorfismus (Polymorphism)

## Encapsulation

> Nešahej mi na data.

Zapouzdření je princip, kdy se snažíme omezit přístup k vlastnostem objektu, abychom předešli chybám. Připomínám, že programování je týmová záležitost (často až stovky vývojářů) a všechny objekty jsou předávány odkazem. Na důležitý objekt pak klidně můžou mít odkaz stovky procesů a projde denně rukama desítkám vývojářů. Každá úprava je pak šíleně riziková, a proto vznikl princip zapouzdření, který se snaží přístup k datům objektu co nejvíc omezit.
 
[Wiki definice](https://en.wikipedia.org/wiki/Encapsulation_%28computer_programming%29)


### Private vs Public

`Public` a `Private` jsou klíčová slova, která pracují s dostupností dat. Např. pokud chceme uchovat cenu auta, ale nechceme ji hned říkat zákazníkovi, můžeme použít něco takového:

``` csharp
public class Car
{
    public int TankCapacity;
    public decimal Consumption;

    private decimal Price;
} 
```

Když pak pracuji s objektem této třídy, `Price` se mi v nabídce ani neukáže. 

![Private](/Images/Private.jpg)

Mezi `private` a `public` pak existuje celá [škála odstínů](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/access-modifiers), ale ty zatím nebudeme vůbec řešit.

K čemu je to dobré? Ukážeme si na příkladu. Vraťte se do Program.cs 



