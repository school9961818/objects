
## Inheritance

Nebo-li dědičnost je další z principů OOP. Slouží primárně k redukci a tedy zpřehlednění dat.

### Example

Krásný případ je klasifikace zvířat v biologii. Abychom si nemuseli u každého zvířete pamatovat úplně všechno, vytvořili jsme stromovu strukturu řazení

```
Kmen
└───Třída
    └───Řád
        └───Čeleď
```
Díky tomu si nemusíme pamatovat vše o `Ostralce bělolící`, stačí vědět, že patří do 
```
Obratlovci
└───Ptáci
    └───Letci
        └───Kachnovití
```
a dokážeme poměrně dobře odhadnout, o jaké zvíře se jedná. ([Link](https://en.wikipedia.org/wiki/White-cheeked_pintail))

### Code example

``` csharp
public class Person
{
    public string Name { get; set; }
}
```

Představme si jednoduchou třídu `Person`. Vytvoříme objekt a vypíšeme na obrazovku.

``` csharp
var student = new Person()
{
  Name = "Kristen Intenselook"
};

Console.WriteLine($"Our first student is {student.Name}. Welcome");
```

Nyní vytvořím třídu pro upíra, která bude dědit z třídy `Person`.

``` csharp
public class Vampire : Person
{
    public bool IsSparkling { get; set; }
}
```
Upír má nyní dvě vlastnosti.
``` csharp
var suspiciousStudent = new Vampire()
{
  Name = "Robert Shovelface",
  IsSparkling = True
};

Console.WriteLine($"Our second student is {suspiciousStudent.Name}. Welcome");
```

Dědičností jsem si ušetřil deklaraci vlastnosti `Name`. Dobře navržená dědičnost mezi objekty dokáže ušetřit spoustu duplicitního kódu -> __přehlednost__.

### Inheritance rules
> Mnoho dětí

Z jedné třídy může dědit libovolné množství dalších tříd

``` csharp
public class Werewolf : Person
{
    public bool IsShirtless { get; set; }
}

var jackedStudent = new Werewolf()
{
  Name = "Jacob Friendzoned",
  IsShirtless = True
};
```

***

> Rodokmen

Je možné dědičnost řetězit přes více generací. V takovém případě dědím od všech předků.

``` csharp
public class Dhampir : Vampire
{
    public bool CanWalkInDaylight{ get; set; }
}

var vampireHunter = new Dhampir()
{
  Name = "Blade",
  IsSparkling = False,
  CanWalkInDaylight = True
};
```

***

> Jeden rodič

Nelze dědit více než z jedné třídy.

``` csharp
public class Nonsense: Vampire, Werevolf
{
  // error
}
```

***

[Do not click this](https://www.youtube.com/watch?v=0gugBiEkLwU)
