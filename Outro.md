## OOP principles

* Zapouzdřenost
* Dědičnost
* Polymorfismus

Ještě jsme si nevysvětlili poslední z principů, i když jsme ho částečně použili.

## Polymorphism

Jedna z vlastností polymorfismu totiž říká, že pokud mám práci pro nějakou třídu/interface, tak mi stačí libovolný potomek.

Např. metodu

``` csharp
public void SendEmailTo(Person person)
{    
}
```

můžu zavolat s objektem třídy `Person`, ale také s libovolným potomkem, tedy `Vampire`, `Student`, atd. Protože každá z nich umí vše, co její předek.

Stejně tak metodu

``` csharp
public void CollectSomeHerbs(IAdventurer adventurer)
{    
}
```

můžu volat se vším, co nějakým způsobem dědí z `IAdventurer` (viz cvičení)

## Definitions

Programování se vyvíjí a s ním i definice jednotlivých vlastností. Často panuje i neshoda, co který princip detailně znamená. (Doporučuji přečíst alespoň wiki)

* https://en.wikipedia.org/wiki/Encapsulation_(computer_programming%29
* https://en.wikipedia.org/wiki/Inheritance_(object-oriented_programming%29
* https://en.wikipedia.org/wiki/Polymorphism_(computer_science%29

Také principy OOP prochází vývojem a po představení `Interface` došlo k jejich iteraci. Nechci říct _nahrazení_, to není úplně pravda, ale ten evoluční skok byl tak veliký, že se vymyslel nový název a principů už nejsou 3 ale 5.

## S.O.L.I.D

Je nejnovější sada (vycházející ze tří původních) principů OOP a platí dodnes. Každé písmeno reprezentuje jeden princip. 

wiki: https://simple.wikipedia.org/wiki/SOLID_(object-oriented_design%29

A to už nechám na Vaše samostudium. Něco je známé, třeba __O - Open/Closed principle__ je jen lépe definovaná zapouzdřenost, ale jiné principy vyžadují k pochopení znalosti nad rámec tohoto kurzu.

## Real life examples

Nyní ovšem máte dostatečné povědomí o C# kódu k tomu, abyste se orientovali i v reálných projektech. Zde několik open-source příkladů.

*** 

[MiniBlog.Core](https://github.com/madskristensen/Miniblog.Core/) (aplikace pro správu blogu)

namátkou: https://github.com/madskristensen/Miniblog.Core/blob/master/src/Services/MetaWeblogService.cs

poznáte co je třída, co interface, kde je constructor? Odhadnete k čemu slouží která metoda?

*** 

[Bitwarden](https://github.com/bitwarden/server) (nástroj pro vývoj mobilních aplikací a serverů)

namátkou: https://github.com/bitwarden/server/blob/master/src/SharedWeb/Utilities/DisplayAttributeHelpers.cs

připomíná vám název nějaký pattern? A je třída static? 

## Where to next

[make money](https://cz.linkedin.com/jobs/junior-dotnet-developer-jobs)

[spent money](https://www.google.com/search?q=dotnet+conferences)

## !!! Importand extended feedback, please give it some love !!!

https://forms.gle/izF5pMGbwcaBeUf36


## Support the author

[Shameless self-promotion](https://www.patreon.com/vojtech_zak)


