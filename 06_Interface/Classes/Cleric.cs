﻿using _06_Interface.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Interface.Classes
{
    public class Cleric : IHealer
    {
        public void Heal()
        {
            Console.WriteLine("By the holy power let me mend your flesh.");
        }

        public void SayGreetings()
        {
            Console.WriteLine("May the light shine on your path.");
        }
    }
}
