﻿using _06_Interface.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Interface
{
    public static class QuestHelpers
    {
        public static void HealInjuredSoldier(IHealer healer)
        {
            healer.SayGreetings();
            healer.Heal();
            Console.WriteLine();
        }

        public static void CollectSomeHerbs(IAdventurer[] adventurers)
        {
            foreach (var adventrurer in adventurers)
            {
                adventrurer.SayGreetings();
            }
            Console.WriteLine("We found some herbs");
            Console.WriteLine();
        }

    }
}
