﻿/*
 * 01 - Přečtěte si https://gitlab.com/school9961818/objects/-/blob/master/Interface.md
 */

using _06_Interface;
using _06_Interface.Classes;

var atalyaSamiteMaster = new Cleric();
QuestHelpers.HealInjuredSoldier(atalyaSamiteMaster);

/*
 * 02 - Seznamte se s kódem.
 * Přesuňte se do třídy "WoodElf" a vyřešte všechny instrukce uvnitř.
 */

/*
 * 03 - 
 * Vytvořte nový objekt třídy "WoodElf". 
 * Zavolejte metodu "HealInjuredSoldier" s touto proměnnou.
 * 
 * inspirace na název proměnné - https://gatherer.wizards.com/Pages/Search/Default.aspx?action=advanced&sort=color,color+&color=+[G]+![U]+![W]+![R]+![B]&type=+[%22Creature%22]+[%22Legendary%22]&subtype=+[%22Elf%22]/
 */

/*
 * 04 - Pošlete healery na bylinky
 * Zavolejte metodu "CollectSomeHerbs" a jako parametr předejte clerica i elfa
 */

/*
 * 05 - Tank
 * Vytvořte nový interface "ITank" (ve složce "Interfaces").
 * Tento interface bude dědit interface "IAdventurer" (stejně jako "IHealer")
 * Tento interface bude mít metodu "void StandYourGround()"
 */

/*
 * 06 - Dwarf
 * Vytvořte novou třídu "Dwarf" (ve složce "Classes").
 * Tato třída bude dědit interface "ITank" 
 * Implementujte všechny metody
 */

/*
 * 07 - 
 * Vytvořte nový objekt třídy "Dwarf". 
 * Znovu pošlete na bylinky oba healery a tentokrát s nimi i trpaslíka.
 */

/*
 * 08 - Ranged
 * Vytvořte nový interface "IRanged" dědící od "IAdventurer"
 * Bude mít metodu "void UnleashHell()" reprezentující útok.
 */

/*
 * 09 - vaše volba
 * Vytvořte novou třídu dle vaší volby, která bude implementovat interface "IRanged"
 * Implementujte všechny metody.
 * Vytvořte objekt této třídy.
 */

/*
 * 10 - 
 * Rozšiřte třídu "WoodElf" o interface "IRanged".
 * Bude dědit ze dvou interface.
 * Implementujte všechny metody.
 */

/*
 * 11 - Final boss 
 * Vytvořte nový quest/metodu v třídě "QuestHelpers".
 *   Vstupní parametry
 *     IHealer
 *     ITank
 *     IRanged
 *   Výstupní parametry
 *     void
 *     
 * Uvnitř metody se nejprve všichni dobrodruzi představí a poté proběhne třikrát (!CYKLUS!) následující sekvence:
 *   tank si stoupne a bude držet
 *   ranged začne střílet
 *   healer začne léčit
 *   
 * Zavolejte metodu s kombinací proměnných, které již máte k dispozici.
 */

/*
 * 12 - https://gitlab.com/school9961818/objects/-/blob/master/Outro.md
 */

