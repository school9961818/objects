﻿## Static

Jak název napovídá je něco statického. Co se nemění.

### Static class

Objekt statické třídy se vytvoří při startu programu a to __právě jednou__.

> 1 static class == 1 object

Zároveň nemá žádnou proměnnou. Ke statickému objektu přistupujeme přes název třídy.

### Example

Příklad je třeba `Console`. Když vytvářím konzolovou aplikaci, chci jen jednu konzoli. Ne víc.


``` csharp
public static class Console
{
  //...
}
```

> Schválně zkuste ve Visual Studiu napsat Console a dát F12

Pokud chci pak do konzole něco vypsat, nepoužívám proměnné, ale název třídy

``` csharp
Console.Write("Hello there.")
```

### Objects

Objekty nám umožňují logicky uspořádat hodnoty s metodami. Ale není to _povinné_. 

* Objekt může mít pouze hodnoty
* Objekt může mít pouze metody

### Static objects

Statické třídy se používají relativně často. Například pro uchování nastavení, která se téměř nemění

```csharp
// class without methods

public static class CountrySettings 
{
    public static string CurrencySuffix { get; } = "Kč";

    public static int DecimalPlaces { get; } = 0;
}
```

Nebo pro uchování metod, které chci používat na více místech (často potkáte _helpers, extensions,..._)

```csharp
// class without properties

public static class AccountingHelpers
{
    public static void DisplayLocalizedInvoice(Invoice invoice, string currencySuffix, int decimalPlaces)
    {
      //...
    }
}
```