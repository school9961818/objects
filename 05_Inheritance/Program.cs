﻿using _05_Inheritance;

/*
 * 01 - Přečtěte si https://gitlab.com/school9961818/objects/-/blob/master/Inheritance.md
 */

var guest = new Person("Dwayne Johnson", new DateTime(1972, 05, 02));
Console.WriteLine($"The first visitor is {guest.Name} of age {guest.GetAge()}");


var topStudent = new Student("Samuel Harris Altman", new DateTime(1985, 04, 22), new int[] { 1, 1, 2 });
Console.WriteLine($"Our top student is {topStudent.Name} of age {topStudent.GetAge()}. His grade average is {topStudent.GetAverage()}");

/*
 * 02 - Seznamte se s kódem.
 * Vytvořte novou třídu "Teacher", která bude dědit z třídy "Person".
 * Navíc bude mít vlastnosti pro kabinet a plat.
 * Vytvořte nový objekt typu učitel a vypište na obrazovku 
 *  Jméno
 *  Plat
 *  Kabinet
 *  Věk
 */


/*
 * 03 - Vytvořte novou třídu "ExchangeStudent", která bude dědit z třídy "Student".
 * Navíc bude mít vlastnost "CountryOfOrigin".
 * Vytvořte nový objekt typu ExchangeStudent a vypište na obrazovku 
 *  Jméno
 *  CountryOfOrigin
 *  Věk
 */

/*
 * 04 - Vytvořte novou třídu "FreelanceTeacher", která bude dědit z třídy "Teacher".
 * Jeho kabinet bude vždy "Unassigned" (zařiďte konstruktorem)
 * Navíc bude mít property pro dny, které může učit (DayOfWeek[])
 * Vytvořte nový objekt typu FreelanceTeacher a vypište na obrazovku 
 *  Jméno
 *  Plat
 *  Věk
 *  a dny, které může učit
 */
