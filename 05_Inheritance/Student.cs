﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_Inheritance
{
    public class Student : Person
    {
        // Inherited constructor - můžete kliknout na "base" a dát F12
        public Student(string name, DateTime dateOfBirth, int[] marks) 
            : base(name, dateOfBirth)
        {
            Marks = marks; // protože jméno a datum narození už pořešil "rodič", stačí pořešit známky 
        }

        public int[] Marks { get; set; }

        public double GetAverage()
        { 
            return Math.Round(Marks.Average(), 2);
        }
    }
}
