﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_Inheritance
{
    public class Person
    {
        public string Name { get; init; }

        public DateTime DateOfBirth { get; init; }

        // Constructor
        public Person(string name, DateTime dateOfBirth)
        {
            Name = name;
            DateOfBirth = dateOfBirth;                
        }

        public int GetAge()
        {
            return ((DateTime.Now - DateOfBirth).Days / 365) + 1;
        }
    }
}
