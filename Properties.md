## Fields and Properties

Princip "omezování" přístupu byl tak častým, že se brzy stal přímo součástí programovacích jazyků (Java, C#,...). Vlastnosti objektů se rozdělily na `field` a `property`.

``` csharp
public class Person
{
    private int age; // tohle je field

    public int Age // tohle je property
    {
        get 
        {
            return age;
        }
        set
        {
            if (age > 0)
            {
                age = value;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Person's age cannot be negative");
            }
        }
    }
}
```

S dělením se postupně ustálila spousta pravidel používání. Z nejznámějších:

* Každý `field` má svoji `property` (tvoří dvojice)
* `field` je vždy `private`
* `property` je vždy `public`
* `field` je vždy s malým písmenem
* `property` je vždy s velkým písmenem
* `field` a `property` mají vždy stejný název (liší se jen velikostí prvního písmene)
* `property` vždy definuje tzv __getter__ a __setter__ (metody pro čtení/zápis do příslušného fieldu)
* a další...

Tyto pravidla fungovala skvěle pro omezování/kontrolu, ale pro vlastnosti objektu, které nebylo potřeba nijak _hlídat_, byly na obtíž. Příklad: 

> Mám třídu pro uživatele a chci mu dát možnost zvolit si přezdívku. Není to povinné a může si zvolit co chce.

``` csharp
public class User
{
    private string nickname; // tohle je field

    public string Nickname // tohle je property
    {
        get 
        {
            return nickname;
        }
        set
        {
            nickname = value;
        }
    }
}
```

Sice to funguje a splníme tak principy zapouzdření (field/property, get/set), ale je to mnoho kódu v podstatě pro nic.

Brzy tak vznikla možnost definovat jen `property` (a nechat C#, ať si `field` pořeší interně). Více kdyžtak tu - [Auto Implemented Properties](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties#auto-implemented-properties) 

Vypadá to takto:

```csharp
public class User
{
    public string Nickname { get; set; }
}
```

>Nejlepší z obou světů.

Tímto splňujeme principy zapouzdření 
- [x] přistupujeme k `property` a nešaháme na žádný `field`
- [x] máme definovaný `getter` i `setter` (i prázdné se počítají)

Jako bonus je náš kód mnohem kratší a přehlednější. Tento způsob budu od této chvíle v kurzu používat.
